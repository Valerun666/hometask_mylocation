//
//  CategoryPickerViewController.h
//  MyLocation
//
//  Created by Valerun on 22.03.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryPickerViewController : UITableViewController

@property (nonatomic, strong) NSString *selectedCategoryName;

@end
