//
//  HudView.h
//  MyLocation
//
//  Created by Valerun on 22.03.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HudView : UIView

+ (instancetype)hudInView:(UIView *)view
                 animated:(BOOL)animated;

@property (nonatomic, strong) NSString *text;

@end
