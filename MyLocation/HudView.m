//
//  HudView.m
//  MyLocation
//
//  Created by Valerun on 22.03.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "HudView.h"

@implementation HudView

+ (instancetype)hudInView:(UIView *)view animated:(BOOL)animated
{
    HudView *hudView = [[HudView alloc] initWithFrame:view.bounds];
    
    hudView.opaque = NO;
    
    [view addSubview:hudView];
    view.userInteractionEnabled = NO;
    
    [hudView showAnimated:animated];
    return hudView;
}

- (void)drawRect:(CGRect)rect
{
    const CGFloat boxWidth = 96.0;
    const CGFloat boxHeight = 96.0;
    
    CGRect boxRect = CGRectMake(
                            roundf(self.bounds.size.width - boxWidth) / 2.0,
                            round(self.bounds.size.height - boxHeight) / 2.0,
                            boxWidth,
                            boxHeight);
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:boxRect cornerRadius:10.0];
    [[UIColor colorWithWhite:0.3 alpha:0.8] setFill];
    [roundedRect fill];
 
// image
    UIImage *image = [UIImage imageNamed:@"Checkmark"];
    
    CGPoint imagePoint = CGPointMake(
                                     self.center.x - roundf(image.size.width / 2),
                                     self.center.y - roundf(image.size.height / 2.0) - boxHeight / 8.0);
    [image drawAtPoint:imagePoint];

// text
    NSDictionary *attributes = @{
                                 NSFontAttributeName : [UIFont systemFontOfSize:16],
                                 NSForegroundColorAttributeName : [UIColor whiteColor]
                                 };
    
    CGSize textSize = [self.text sizeWithAttributes:attributes];
    CGPoint textPoint = CGPointMake(
                                    self.center.x - roundf(textSize.width / 2.0),
                                    self.center.y - roundf(textSize.height / 2) + boxHeight / 4.0);
    [self.text drawAtPoint:textPoint withAttributes:attributes];
}

- (void)showAnimated:(BOOL)animated
{
    if (animated) {
        self.alpha = 0.0;
        self.transform = CGAffineTransformMakeScale(1.3, 1.3);
        
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 1.0;
            self.transform = CGAffineTransformIdentity;
        }];
    }
}


@end
