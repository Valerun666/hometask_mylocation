//
//  MyNavigationController.m
//  MyLocation
//
//  Created by Valerun on 06.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "MyNavigationController.h"

@interface MyNavigationController ()

@end

@implementation MyNavigationController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
