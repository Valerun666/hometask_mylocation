//
//  MyTabBarController.m
//  MyLocation
//
//  Created by Valerun on 06.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "MyTabBarController.h"

@interface MyTabBarController ()

@end

@implementation MyTabBarController

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIViewController *)childViewControllerForStatusBarStyle
{
    return nil;
}

@end
