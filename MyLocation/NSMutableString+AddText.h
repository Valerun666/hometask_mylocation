//
//  NSMutableString+AddText.h
//  MyLocation
//
//  Created by Valerun on 06.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (AddText)

- (void)addText:(NSString *)text withSeparator:(NSString *)separator;

@end
