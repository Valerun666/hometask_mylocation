//
//  NSMutableString+AddText.m
//  MyLocation
//
//  Created by Valerun on 06.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "NSMutableString+AddText.h"

@implementation NSMutableString (AddText)

- (void)addText:(NSString *)text withSeparator:(NSString *)separator
{
    if (text != nil) {
        if ([self length] > 0) {
            [self appendString:separator];
        }
        [self appendString:text];
    }
}

@end
