//
//  UIImage+Resize.h
//  MyLocation
//
//  Created by Valerun on 06.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)resizeImageWithBounds:(CGSize)bounds;

@end
